

MAIN_PATH = '/datadrive/Repos'

import time
import math
import os
import pandas as pd
import pymssql
from sqlalchemy import create_engine

from collections import Counter
from copy import copy


#Only used for merging AFTER the Database class
def merge_table(table_map):
    target_col = 'randomrandomrandomrandmorandomasodsadiajsdlkaa'
    new_list = []
    for df, col_name in table_map:
        df = df.assign(**{target_col: getattr(df, col_name)})
        new_list.append(df)
    ldf, rdf = new_list
    ndf = ldf.merge(right = rdf, on = target_col, how = 'left')
    ndf = ndf.drop(target_col, axis = 1)
    return ndf

#Functions for the mapping excels.
def open_object_relation_map_excel(fn):
    return [pd.read_excel(fn, sheetname = i) for i in range(3)]

def get_similar_table_names(objects, table_name):
    return list(set([i for i in objects if table_name in i]))

def get_cursor_to_dataframe(cursor):
    df = pd.DataFrame(cursor.fetchall())
    if len(df) > 0:
        df.columns = [i[0] for i in cursor.description]
    return df




#################################
def get_table_name_into_df(cursor, table_name):
    cursor.execute('SELECT * FROM {table_name}'.format(table_name = table_name))
    df = get_cursor_to_dataframe(cursor)
    return df

def write_df_to_csv(df, database_name, table_name):
    path = os.path.join(MAIN_PATH, 'data', database_name, '{}.csv'.format(table_name))
    try:
        df.to_csv(path, encoding = 'utf-8')  
        print 'written', table_name
    except UnicodeDecodeError:
        df = fix_decoding_dataframe(df)
        df.to_csv(path, encoding = 'utf-8')  
        print 'written error corrected df'
        
def get_tables(cursor, database_name, table_list):
    for table_name in table_list:
        df = get_table_name_into_df(cursor, table_name)
        print 'downloaded', table_name
        write_df_to_csv(df, database_name, table_name)

def get_all_columns_for_partial_table_name(cursor, partial_table_name, objects):
    similar_table_names = get_similar_table_names(objects.ObjectName, partial_table_name)
    data_list = []
    for table_name in similar_table_names:
        print table_name
        cursor.execute("SELECT * FROM {table_name}".format(table_name = table_name))
        df = get_cursor_to_dataframe(cursor)
        data_list.append(df)
    merged_df = pd.concat(data_list)
    return merged_df.drop_duplicates()

def get_and_write_partial_table_names(cursor, database_name, table_list, objects):
    for table_name in table_list:
        df = get_all_columns_for_partial_table_name(cursor, table_name, objects)
        write_df_to_csv(df, database_name, table_name)

        
        
        
    
#Functions for cleaning and joining
def rename_col(database_name, df_name, col_name):
    return database_name + '_' + df_name.upper() + '_TABLE_' + col_name

def rename_df(database_name, df, df_name, target_column, multiple_columns = None):
    if multiple_columns is not None:
        new_columns = {col_name: rename_col(database_name, df_name, col_name) for col_name in df.columns if col_name not in multiple_columns}
    else:
        new_columns = {col_name: rename_col(database_name, df_name, col_name) for col_name in df.columns if col_name is not target_column}
    new_df = pd.DataFrame()
    for col in new_columns:
        new_df[new_columns[col]] = df[col]
    new_df[target_column] = df[target_column]
    if multiple_columns is not None:
        for c in multiple_columns:
            if c in df.columns:
                new_df[c] = df[c]
    return new_df


def download_all_partial_table_names(cursor, database_name, table_names):
    table_list = []
    for table_name in table_names:
        get_all_columns_for_partial_table_name
        similar_table_names = get_similar_table_names(objects.ObjectName, table_name)
        for similar_table_name in similar_table_names:
            table_list.append(similar_table_name)
    get_tables(cursor, database_name, table_list)
    
def open_df(db_name, table_name):
    return pd.read_csv(os.path.join(MAIN_PATH, 'data', db_name, table_name + '.csv')).drop('Unnamed: 0', axis = 1)

def open_rename_df(db_name, table_name, key, not_rename_columns):
    path = os.path.join(MAIN_PATH, 'data', db_name, table_name + '.csv')
    print "opening: ", path
    product_df = pd.read_csv(path).drop('Unnamed: 0', axis = 1)
    return rename_df(db_name, product_df, table_name, key, not_rename_columns)

def drop_null_cols(df):
    for c in [c for c in df.columns if all(df[c].isnull())]:
        if c in df.columns:
            df = df.drop(c, axis = 1)
    return df

def drop_columns(df, columns):
    for c in columns:
        try:
            df = df.drop(c, axis = 1)
            print df.shape
        except ValueError:
            print c
    return df

def fix_decoding_dataframe(df):
    for c in df.columns:
        df[c] = map(lambda x: (x.decode('latin-1') if type(x) == str else x), df[c])
    return df

def select_keep_columns(df, keep_columns):
    df_cols = df.columns
    matched_cols = []
    for c in keep_columns:
        if c in df_cols:
            matched_cols.append(c)
    return df[matched_cols]


    
class WriteDatabase(object):
    
    
    def __init__(self, engine_args):
        self.engine_args = engine_args
        self.connection_string = "{db_type}://{user}:{password}@{address}/{db_name}".format(**self.engine_args)
        self.engine = create_engine(self.connection_string, encoding='latin1', echo=False)
        
    def write_df_to_db(self, df, table_name, if_exists =  'replace',):
        df.to_sql(table_name, self.engine, if_exists = if_exists, chunksize = 100000)

        

class OpenDatabase(object):
    
    
    def __init__(self, *args, **kwargs):
        self.download_data = True
        self.data_tables = []
        self.id_tables = []
        self.database_name = 'isisdatabase'
        self.table_relations = {}
        self.table_order = self.table_relations.keys()
        self.init_table_name = None
        self.do_not_rename_cols = []
        self.init_table = None
        self.merged_table = copy(self.init_table)
        self.table_rules = {}
        self.download_data_func_dict = {}
        self.id_mapping_keys = []
        self.id_mapping = []
        self.keep_column_names = {}
        
        if kwargs is not None:
            for attr, value in kwargs.items():
                setattr(self, attr, value)
        if hasattr(self, 'db_dict'):
            self.objects, self.relations, self.sizes = open_object_relation_map_excel(self.db_dict[self.database_name])
        if self.download_data:
            self.init_cursor()
        if self.keep_column_names != {}:
            self.complete_keep_column_names()
            
    def init_cursor(self):
        conn = pymssql.connect(self.sql_args['address'], 
                              self.sql_args['user'],
                              self.sql_args['password'],
                              self.database_name)
        self.cursor = conn.cursor()
        print self.cursor, self.database_name
        
    def complete_keep_column_names(self):
        for col_name in self.do_not_rename_cols:
            for key in self.keep_column_names:
                self.keep_column_names[key].append(col_name)
                

    def fetch_tables(self):
        ### download and write
        #get_tables(self.cursor, self.database_name, self.data_tables)
        if self.download_data:
            for func, args in self.download_data_func_dict.items():
                #print func, eval(args)
                eval(func)(*eval(args))

    def set_init_table(self):
        main_key, _, _, _ = self.table_relations[self.init_table_name]
        self.init_table = open_rename_df(self.database_name, self.init_table_name, main_key, self.do_not_rename_cols)
        self.init_table = self.apply_table_rule(self.init_table_name, self.init_table)
        self.init_table = self.apply_keep_column_names(self.init_table_name, self.init_table)
        
    def apply_keep_column_names(self, table_name, df):
        if table_name in self.keep_column_names:
            df = select_keep_columns(df, self.keep_column_names[table_name])
        return df
        
    def apply_table_rule(self, table_name, table_df):
        if table_name in self.table_rules:
            table_df = self.table_rules[table_name](table_df)
        return table_df
    
    def get_relevant_id_keys(self, id_map_df, table_name):
        relevant_id_keys = []
        for key in self.id_mapping_keys[table_name]:
            if key in id_map_df.columns:
                relevant_id_keys.append(key)
        return relevant_id_keys
    
    def apply_id_mapping(self):
        print "apply_id_mapping"
        s = time.time()
        def replace_value(value):
            if value is not None:
                if type(value) == str:
                    if value in self.val_dict:
                        return self.val_dict[value]
                    else:
                        return value
                elif (type(value) == int or type(value) == float) and not math.isnan(value):
                    if float(value) in map(float, self.val_dict.keys()):
                        return self.val_dict[value] 
    
            return value

        for table_name in self.id_mapping:
            target_column = self.id_mapping[table_name]

            id_map_df = open_df(self.database_name, table_name)
            id_mapping_keys = self.get_relevant_id_keys(id_map_df, table_name)
            print "ID MAPPING: {}".format(table_name), time.time() - s, id_mapping_keys

            self.val_dict = dict(id_map_df[id_mapping_keys].values)
            
            self.init_table[target_column] = self.init_table[target_column].apply(replace_value)  
        
    def apply_column_name_cleaning(self):
        print "apply_column_name_cleaning"
        self.init_table = self.clean_df_col_names(self.init_table)
        
    def get_date_cols(self): #Split?
        date_cols = []
        if hasattr(self, 'date_columns'):
            for col in self.date_columns:
                date_cols.append(col)
                
        for col in self.init_table.columns:
            if 'date' in col.lower():
                date_cols.append(col)       
        return date_cols
    
    def apply_date_conversion(self):
        print "apply_date_conversion"
        date_cols = self.get_date_cols()
        for col in date_cols:
            self.init_table = self.init_table.assign(**{col: self.init_table[col].apply(pd.to_datetime, errors = 'coerce')})               

    def type_check_merge(self, main_table, right_table, key):
        c1, c2 = Counter(map(type, main_table[key])), Counter(map(type, right_table[key]))
        type_check_bool = c1.keys()[0] == c2.keys()[0] 
        print type_check_bool, c1, c2
        return type_check_bool, c1, c2
    
    def cast_type(self, main_table, right_table, key):
        type_check_bool, c1, c2 = self.type_check_merge(main_table, right_table, key)
        if not type_check_bool:
            right_table[key] = right_table[key].astype(c1.keys()[0])
        type_check_bool, c1, c2 = self.type_check_merge(main_table, right_table, key)
        return main_table, right_table
    
    def clean_string(self, string):
        if 'TABLE' in string:
            return string[string.index('TABLE') + len('TABLE') + 1:]
        else:
            return string

    def clean_df_col_names(self, df):
        new_df = pd.DataFrame()
        for col in df.columns:
            if len(df[col].shape) > 1:
                new_df[self.clean_string(col)] = df[col].T[:1].T
            else:
                new_df[self.clean_string(col)] = df[col]
        return new_df

    def check_double_target_keys(self, left_df, right_df, right_key, left_key):
        if right_key in left_df.columns:
            right_df = right_df.drop(right_key, axis = 1)
        return right_df
        
    def merge_relations(self): 
        for table_name in self.table_order:
            new_table_key, init_key, retarget_key, merge_how = self.table_relations[table_name]
            new_table_df = open_rename_df(self.database_name, table_name, new_table_key, self.do_not_rename_cols)
            new_table_df = self.apply_table_rule(table_name, new_table_df)
            new_table_df = self.apply_keep_column_names(table_name, new_table_df)
            #print table_name, new_table_df.columns.values, self.init_table.columns.values
            
            new_table_df[retarget_key] = getattr(new_table_df, new_table_key).dropna()
            self.init_table[retarget_key] = getattr(self.init_table, init_key)
            self.init_table, new_table_df = self.cast_type(self.init_table, new_table_df, retarget_key)
            new_table_df = self.check_double_target_keys(self.init_table, new_table_df, new_table_key, init_key)
            print table_name, new_table_df.shape, self.init_table.shape

            self.init_table = self.init_table.merge(right = new_table_df, 
                                                    on = retarget_key, 
                                                    how = merge_how)
            print 'after', table_name, self.init_table.shape, new_table_df.shape
        
        print "Done merging"
        
   
    def apply_func_to_init_table(self, func):
        self.init_table = func(self.init_table)
        
        
