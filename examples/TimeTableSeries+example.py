# coding: utf-8

import os
import pandas as pd
import timelord as tl

TTS=tl.table_timeseries.TableTimeSeries()

def dump_df_hts(df,head=3,tail=3,shape=True):
    print("HEAD3:")
    print(df.head(head))
    print("TAIL3:")
    print(df.tail(tail))
    if shape:
        print("SHAPE:")
        print(df.shape)

path = '../test_data/'
review_df = pd.read_csv(os.path.join(path, 'amazon_reviews.csv')).drop('Unnamed: 0', axis = 1)
#dfhts(review_df)

#The date column isn't in the right format yet so we cast it into a datetime series. From the head it looks like unix datetime
review_df['Time'] = review_df['Time'].apply(lambda x: pd.to_datetime(x, unit = 's'))
dump_df_hts(review_df)


#EXAMPLE 1: Create a multiple_date operations_dict based on the types in 
#Goal: Test the "map_dtype_func_map" method
#Input: review_df
#output column_func_map

from timelord.util import Util

def agg_func(x):
    return list(x)

INPUT_DF = review_df #The dataframe to which the columns must be matched
dtype_func_map = {'int': 'mean', #Input
                 'object': agg_func,
                 'datetime': 'unique'}

multiple_date_operations = Util.map_dtype_func_map(INPUT_DF, dtype_func_map)
multiple_date_operations['Id'] = agg_func

#multiple_date_operations #The desired output
#dfhts(review_df)


#EXAMPLE 2: Test the multiple date operations

#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'day',
                     multiple_date_operations = multiple_date_operations)
dump_df_hts(new_df)#The desired output


#EXAMPLE 3: Test the multiple date operations
#NOTICE THE DOUBLE DATES at the first 2 rows.
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'day',
                     multiple_date_operations = False)
dump_df_hts(new_df)#The desired output


#EXAMPLE 4: Test the multiple date operations
#NOTICE THE dropped columns due to conflicting operations (it defaults to .sum which can't be done on objects
# so those are silently dropped)
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'day',
                     multiple_date_operations = True)
dump_df_hts(new_df)#The desired output


#EXAMPLE 5: Test the resolution on year.
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'year',
                     multiple_date_operations = multiple_date_operations)
dump_df_hts(new_df)

#EXAMPLE 6: Test the resolution on month.
#Input = review_df

new_df = TTS.build_df(df = review_df,
                     date_column = 'Time',
                     min_resolution = 'month',
                     multiple_date_operations = multiple_date_operations)
dump_df_hts(new_df)



#EXAMPLE 7: build_df_per_id method: test without multiprocessing WITH multiple_date_operations = True
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = False,
                             multiple_date_operations = True)
dump_df_hts(new_df) 



#EXAMPLE 8: build_df_per_id method: test without multiprocessing WITH multiple_date_operations = False
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = False,
                             multiple_date_operations = False)
dump_df_hts(new_df) 



#EXAMPLE 9: build_df_per_id method: test without multiprocessing WITH multiple_date_operations = multiple_date_operations
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = False,
                             multiple_date_operations = multiple_date_operations)
dump_df_hts(new_df) 


#EXAMPLE 10: build_df_per_id method: test WITH multiprocessing = True WITH multiple_date_operations = False
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = True,
                             multiple_date_operations = False)
dump_df_hts(new_df) 


#EXAMPLE 11: build_df_per_id method: test WITH multiprocessing = True WITH multiple_date_operations = True
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = True,
                             multiple_date_operations = True)
dump_df_hts(new_df) 




#EXAMPLE 12: build_df_per_id method: test WITH multiprocessing = True WITH multiple_date_operations = True
#

new_df = TTS.build_df_per_id(df = review_df,
                             date_column = 'Time',
                             min_resolution = 'day',
                             id_column = 'UserId',
                             use_multiprocessing = True,
                             multiple_date_operations = multiple_date_operations)
dump_df_hts(new_df) 
