# -*- coding: UTF-8 -*-
import sys


FEATURE_TEMPLATE='''
Feature: {name}
Scenario: {name}
    Given a file named "run_script.py" with:
        """
{indented_snip}
        """
    Given a file named "run_and_save_output.sh" with:
        """
        python run_script.py > run_result.txt 2>&1
        """        
    When I run "sh run_and_save_output.sh"
    When I run "cp ../test_data/expected_outputs/{expexted_output_txt_file} expected_output.txt"
    When I run "diff --report-identical-files expected_output.txt run_result.txt"
    Then the command output should contain "Files expected_output.txt and run_result.txt are identical"
    And the command returncode is "0"

'''

if __name__=='__main__':
     if len(sys.argv) in (4,5):
        fname = sys.argv[1]   # 'new feature'
        ffrom = sys.argv[2]   # 'cool_snippet.py'
        fexpect = sys.argv[3] # '../test_data/expected_outputs/{expexted_output_txt_file}'
        fexpect = fexpect.replace('../test_data/expected_outputs/','') #help a little
        indent=' '*8
        snippet=''
        save=False
        if len(sys.argv)==5 and sys.argv[4]=='--save':
            save=True

        with open(ffrom) as f:
            for line in f:
                snippet+=indent+line
        feature = FEATURE_TEMPLATE.format(name=fname,
                                          indented_snip=snippet,
                                          expexted_output_txt_file=fexpect)
        if save:
            feature_file='features/'+fname+'.feature'
            with open(feature_file, 'w') as f:
                f.write(feature)
            print('written: '+feature_file)
            print('please check if feauture is behaving correct, run:')           
            print('behave '+feature_file)
            
        else:
            print(feature)
     else:
        print("usage: " + sys.argv[0] + "usage new_feature_name snippet_file expected_output_file")
        print('please check generated output')
        print('to save(overwrite) feature run with a --save option appended:')
        print(sys.argv[0] + "usage new_feature_name snippet_file expected_output_file --save")
        
       
