Feature: Timelord Table Time series load
Scenario: Timelord Table Time series load
    Given a file named "run_script.py" with:
            """
            import pandas as pd
            import numpy as np
            import timelord as tl
            #this example does not use tl yet.
            
            import os
            
            path = '../test_data'
            review_df = pd.read_csv(os.path.join(path, 'amazon_reviews.csv')).drop('Unnamed: 0', axis = 1)
            
            #review_df.head(3)

            #The date column isn't in the right format yet so we cast it into a datetime series.
            # From the head it looks like unix datetime
            review_df['Time'] = review_df['Time'].apply(lambda x: pd.to_datetime(x, unit = 's'))
            
            #print(review_df)
            print("HEAD3:")
            print(review_df.head(3))
            print("TAIL3:")
            print(review_df.tail(3))
            print("SHAPE:")
            print(review_df.shape)
            """
    Given a file named "run_and_save_output.sh" with:
            """
            python run_script.py > run_result.txt
            """        
    When I run "sh run_and_save_output.sh"
    When I run "cp ../test_data/expected_outputs/time_table_series_1_feature_expected_output.txt expected_output.txt"
    When I run "diff --report-identical-files expected_output.txt run_result.txt"
    Then the command output should contain "Files expected_output.txt and run_result.txt are identical"
    And the command returncode is "0"
