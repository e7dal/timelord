
Feature: hello
Scenario: hello
    Given a file named "run_script.py" with:
        """
        print('hello new feature')
        m=[]
        for i in range(3):
            m.append([])
            for j in range(4):
                m[i].append(i+j)
        print(m)
            
        """
    Given a file named "run_and_save_output.sh" with:
        """
        python run_script.py > run_result.txt
        """        
    When I run "sh run_and_save_output.sh"
    When I run "cp ../test_data/expected_outputs/hello_output.txt expected_output.txt"
    When I run "diff --report-identical-files expected_output.txt run_result.txt"
    Then the command output should contain "Files expected_output.txt and run_result.txt are identical"
    And the command returncode is "0"

