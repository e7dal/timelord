Feature: Timelord util 
Scenario: Timelord 
    Given a file named "run_script.py" with:
            """
            import pandas as pd
            import numpy as np
            import timelord as tl
            def agg_func(x):
                return list(x)

            dtype_func_map = {'int': 'sum',
                              'float': 'sum',
                              'object': agg_func,
                              'datetime': 'unique'}
            #trivial df example
            df = pd.DataFrame({'int' : [4,5,6,7], 
                               'float' : [10.1,20.2,30.3,40.4],
                               'object': ['one','two','three','four'],
                               'datetime' : [ pd.Timestamp('20171020'),
                                              pd.Timestamp('20171021'),
                                              pd.Timestamp('20171022'),
                                              pd.Timestamp('20171023')]})
            res=tl.utils.util.Util.map_dtype_func_map(df, dtype_func_map)

            print(res)
            """
    When I run "python run_script.py"
    Then the command output should contain "{'datetime': 'unique', 'float': 'sum', 'int': 'sum', 'object': <function agg_func at "
    And the command returncode is "0"
