import numpy as np
import pandas as pd
from collections import defaultdict

from statsmodels.tsa.api import VAR
from statsmodels.tsa.stattools import adfuller, kpss

def calc_RMSLE(y_pred, y_true):
    return np.sqrt(np.mean((np.log(y_pred + 1) - np.log(y_true + 1))**2))


class MultiVarForecast:    
    """      
    WARNING: 
    This class is still heavily under development. 
    Docs & Tests are incoming and most functions are bound to change. 
    
    This class receives as input a Dataframe with multiple time series (with DatetimeIndex as index) that are correlated. 
    A VAR model is fitted & provides a forecast for the specified column with confidence intervals with an error as specified by the alpha parameter.
    
    This class heavily relies upon: http://www.statsmodels.org/devel/vector_ar.html
    
  
    TODO:
    > Test full corr columns droppping --> VAR is still raising errors
    > Test constant columns
    > add some simple benchmark models
    > Catch NaN values nicer
    > Build support for multivariate forecasting with confidence intervals and returning a dataframe with the right column names.
    > expose VAR model for further analysis
    > add more evaluation criteria function argument to the model fitter.
    
    > add "impulse" investigation methods
    
    > break class in Util, Forecast class, RollingValidation
    
    TODO (outside this class):
        > contribute to statsmodels to catch the negative errors occuring in forecast_interval due to negative errors 
        from VAR.results._forecast_vars 
        
        error raised in:
        statsmodels/tsa/vector_ar/var_model.py:739: RuntimeWarning: invalid value encountered in sqrt
          sigma = np.sqrt(self._forecast_vars(steps))
        
        error raised on negative values which is probably caused 
        by massive variance in the last few timesteps (years of zero and sudden spike)  
    """
    
    def __init__(self, datetime_index_df, fc_column = None, drop_constant_columns = True, drop_full_cor_columns = True, do_log_transform = True, round_forecast_up = True, clip_data_before_fc = True, cut_off_data_from_start_fc_column_percentage = 0.0, drop_late_variance_start_column_threshold_percentage = 0.0):
        self.datetime_index_df = datetime_index_df #init df
        self.fc_column = fc_column
        self.drop_constant_columns = drop_constant_columns #bool
        self.drop_full_cor_columns = drop_full_cor_columns #bool
        self.do_log_transform = do_log_transform #bool
        self.round_forecast_up = round_forecast_up #bool
        self.clip_data_before_fc = clip_data_before_fc #bool
        
        #The below values are not optimal in use perhaps... If 
        self.cut_off_data_from_start_fc_column_percentage = cut_off_data_from_start_fc_column_percentage #range from (1, 0) where 0 is no cut-off
        self.drop_late_variance_start_column_threshold_percentage = drop_late_variance_start_column_threshold_percentage #if 0 skip function
        #all columns with a late variance threshold ABOVE this are dropped
        


        self.before_log_correction_val = 0 #Default val
    
    def set_before_log_correction_val(self, df):
        """ 
        Translates the data based on the absolute lowest value which makes the lowest value
        in the dataset 0. After that it adds +2 to it as to prevent log(0) from happening.
        and sets it to the attribute before_log_correction_val.
        
        Note: +2 seems to work more stable than +1 though this can use solid proof.
        
        Parameters
        ----------
        df : pandas DataFrame
        The input data, which often is self.datetime_index_df
        
        Returns
        ----------
        None
        """
        self.before_log_correction_val = abs(min(df.min().values)) + 2 #+1 seems to tricky

    def do_log_transform_func(self, df):
        """ 
        Sets and applies the before_log_correction after which it will log transform the data.
                
        Parameters
        ----------
        df : pandas DataFrame
        The input data, which often is self.datetime_index_df
        
        Returns
        ----------
        df : pandas DataFrame
        """
        self.set_before_log_correction_val(df)
        df += self.before_log_correction_val
        df = np.log(df)
        return df
    
    def undo_log_transform_func(self, array):
        """ 
        undoes the log transform by raising the values against an exponent and undoing the before_log_transform
        as to get back to the original values.
                
        Parameters
        ----------
        array : numpy array or pandas DataFrame
        The input data, which often is self.datetime_index_df
        
        Returns
        ----------
        array : numpy array or pandas DataFrame
        """       
        return np.exp(array) - self.before_log_correction_val
        
    def __get_constant_columns(self, df): #mVarForecast util
        """ 
        Iterates over each column to subtract the largest value and smallest value from each other.
        If this value equals zero than the entire column is a constant on which the model will raise
        an error. This method returns the index where this occurs.
        
                
        Parameters
        ----------
        df : pandas DataFrame
        The input data, which often is self.datetime_index_df
        
        Returns
        ----------
        array : numpy array
        Contains the column list indices of the constant columns. These can be dropped afterwards.
        """       
        return np.where(df.apply(lambda x: x.max() - x.min()).values == 0)[0]

    def __drop_constant_columns(self, df, array):
        """ 
        Iterates over each column to subtract the largest value and smallest value from each other.
        If this value equals zero than the entire column is a constant on which the model will raise
        an error. This method returns the index where this occurs.
        
                
        Parameters
        ----------
        df : pandas DataFrame
        The input data, which often is self.datetime_index_df

        array : pandas DataFrame
        The to be dropped column indices.
        
        Returns
        ----------
        df : pandas DataFrame
        """       
        for i, j in enumerate(array):
            df = df.drop(df.columns[j], axis = 1)
        return df

    def clean_constant_columns(self, df): #mVarForecast util
        """ 
        See combination of self.__drop_constant_columns() and self.__get_constant_columns()
        
                
        Parameters
        ----------
        df : pandas DataFrame
        The input data, which often is self.datetime_index_df
         
        Returns
        ----------
        ndf : pandas DataFrame
        """       
        c = self.__get_constant_columns(df)
        ndf = self.__drop_constant_columns(df, c)
        return ndf

    def drop_full_corr_columns(self, df): #mVarForecast util
        """ 
        Computes the correlation matrix of the input dataframe to identify the columns
        that have a complete correlation (corr == 1) with another column. These are dropped as to prevent the model
        from raising errors / speeding up the training.
                
        Parameters
        ----------
        df : pandas DataFrame
        The input data, which often is self.datetime_index_df
         
        Returns
        ----------
        ndf : pandas DataFrame
        """       
        x, y = np.where(df.corr().values == 1)
        org_cols = []
        drop_cols = []
        for (x, y) in zip(x, y):
            if x not in org_cols and x not in drop_cols:
                org_cols.append(x)
                drop_cols.append(y)

            elif y not in org_cols:
                drop_cols.append(y)
        ndf = df[[df.columns[i] for i in org_cols]]
        return ndf
    
    def get_ts_index_var_start(self, series):
        """
        WARNING: Needs testing in practice to test performance!
        Assuming the last occured values in the to-forecast series is non-zero:
        This method computes the variance of a series to create a spike for when the actual time series start.
        The variance is used to catch some minor noise before the actual time series start - maybe a bit overkill.
        This variance series is reversed to make computation of the to-get index easier.
        The index of the above variance threshold values are computed
        The first derivative of this index list is computed to identify large spikes, which imply a larger
        than X near-zero variance space between the to-forecast df and other data.
        The index value before the large spike is the last value of the to-forecast series.
        If multiple large spikes are caught the first occuring value will be used.
        
        Parameters
        ----------
        series : pandas Series
         
        Returns
        ----------
        zero_var_start_index : integer
        The index from where the variance probably starts.
        """
        vals = series.rolling(window = 7).var().dropna()

        variance_threshold = np.median(vals)*0.5 #magic number. Seemed to work out the best in practice.
        variance_threshold_space_threshold = 1
        
        more_than_zero_var_indices = np.where(vals[::-1] > variance_threshold)[0]
        index_to_forecast_data = np.where(np.diff(more_than_zero_var_indices) > variance_threshold_space_threshold)[0]
        if len(index_to_forecast_data) > 1: #No other variance in the series is found except for the msot recent one.
            index_to_forecast_data = index_to_forecast_data[0] #The first value is the first data 
        else:
            index_to_forecast_data = more_than_zero_var_indices[-1]
        print("INDEX" , index_to_forecast_data, len(series))
        ts_index_start = len(series) - index_to_forecast_data
        return ts_index_start #The index is based on the reverse so 


    def get_variance_column_percentage(self, series, ts_index_start):
        """        
        Computes the percentage of the variance column next to the total length.
        This is used for an average treshold.
        
        Parameters
        ----------
        series : pandas Series
        
        ts_index_start : integer
         
        Returns
        ----------
        variance_column_percentage : float
        """
        return ts_index_start / len(series)
    
    def drop_columns_below_variance_start(self, datetime_index_df):
        """
        WARNING: Unpredictable results as of yet. Do not use.

        Iterates over the columns to see if the variance starts later relatively from the 
        set threshold. If so the column will be dropped. 
        WARNING: Needs testing in practice to test performance!
        WARNING: Do not use recursively.
        
        Parameters
        ----------
        datetime_index_df : pandas DataFrame
                 
        Returns
        ----------
        datetime_index_df : pandas DataFrame
        """
        keepcols = []
        for col in datetime_index_df.columns:
            series = datetime_index_df[col]
            ts_index_var_start = self.get_ts_index_var_start(series)
            percentage = self.get_variance_column_percentage(series, ts_index_var_start)
            if percentage < self.drop_late_variance_start_column_threshold_percentage:
                keepcols.append(col)
        return datetime_index_df[keepcols]

    def cut_off_data_from_start_fc_column(self, datetime_index_df):
        """
        WARNING: Unpredictable results as of yet. Do not use.
        
        
        Cuts off the entire dataset based on where the variance in the to-forecast column starts.
        This can be useful for dataframes where the variance starts very late on and the model
        has a hard time fitting based on a long series of zero values.
        WARNING: Needs testing in practice to test performance!
        
        Parameters
        ----------
        datetime_index_df : pandas DataFrame
                 
        Returns
        ----------
        datetime_index_df : pandas DataFrame
        """
        series = self.datetime_index_df[self.fc_column]
        ts_index_var_start = self.get_ts_index_var_start(series)
        cut_off = int(ts_index_var_start * self.cut_off_data_from_start_fc_column_percentage)
        return datetime_index_df.iloc[cut_off:, :]
            
    def test_stationarity_columns(self, df, use_kpss = True, use_adfuller = False): #mVarForecast util
        #Adfuller takes approx 100x as long as kpss
        #by default only returns p_val
        #stationairity with kpss if p_val is >.05
        #stationarity with adfuller if p_val is <.05
        #Doesnt seem necessary in general? If it is, todo : write function to filter below threshold values.
        
        """        
        Parameters
        ----------
        datetime_index_df : pandas DataFrame
                 
        Returns
        ----------
        datetime_index_df : pandas DataFrame
        """
        stationarity_dict = defaultdict(dict)
        for col in df.columns:
            if use_kpss:
                val = kpss(df[col])
                stationarity_dict[col]['kpss'] = val[1]
            if use_adfuller:
                val = adfuller(df[col])
                stationarity_dict[col]['adf'] = val[1]
        return stationarity_dict
    
    def build_prob_array(self, fc, index):
        point, lower, upper = fc
        fcs = []
        for i in range(len(point)): #Speed this up by using the arrays and array indices
            fcs.append((point[i][index], lower[i][index], upper[i][index]))
        return fcs

    def build_fc_df(self, data, fcs): #A bit crude, no?
        fc_length = len(fcs)
        start = data.index[-1]
        date_range = pd.date_range(start = start, periods = fc_length, freq = pd.infer_freq(data.index))
        df = pd.DataFrame(fcs, columns = ['point_forecast', 'low', 'high'])
        df.index = pd.DatetimeIndex(date_range)
        return df

    def prep_data_for_forecast(self, datetime_index_df):
        print("Start clean", datetime_index_df.shape)
        if self.clip_data_before_fc:
            datetime_index_df = datetime_index_df.clip(lower = 0) #make parametrizable?
            
        if self.cut_off_data_from_start_fc_column_percentage and self.fc_column:
            datetime_index_df = self.cut_off_data_from_start_fc_column(datetime_index_df)
        print("Cut off", datetime_index_df.shape)
        
        if self.drop_late_variance_start_column_threshold_percentage:
            datetime_index_df = self.drop_columns_below_variance_start(datetime_index_df)
        print("Drop columns below threshold", datetime_index_df.shape)
        
        if self.drop_constant_columns:
            datetime_index_df = self.drop_full_corr_columns(datetime_index_df)
        print("Drop constants", datetime_index_df.shape)
        
        if self.drop_full_cor_columns:
            datetime_index_df = self.clean_constant_columns(datetime_index_df)
        print("Drop full cor", datetime_index_df.shape)
        
        if self.do_log_transform:
            datetime_index_df = self.do_log_transform_func(datetime_index_df) 
        return datetime_index_df

    def setVARresults(self, lag_order, forecast_length, alpha = 0.05):
        self.model = VAR(self.datetime_index_df)
        self.results = self.model.fit(lag_order)

    def transform_forecast(self, points):
        if self.do_log_transform:
            points = self.undo_log_transform_func(points)
        if self.round_forecast_up: #All forecast values below zero are rounded up >=0
            points = points.clip(min=0)
        return points

    def make_forecast(self, lag_order = 14, forecast_length = 14, alpha = None, prep_data = True):
        if prep_data:
            self.datetime_index_df = self.prep_data_for_forecast(self.datetime_index_df)
        self.setVARresults(lag_order, forecast_length)
        if alpha:
            #NOT IMPLEMENTED
            #Used to get confidence interval. Can raise errors when confidence interval goes into negative values 
            #due to the np.sqrt call in the original method. Can be ignored with caution.
            y_pred, lower, upper = self.results.forecast_interval(self.datetime_index_df.values[-lag_order:], 
                                                                    forecast_length,
                                                                     alpha = alpha)
        else:
            y_pred = self.results.forecast(self.datetime_index_df.values[-lag_order:], 
                                            forecast_length)
        if self.fc_column:
            forecast_column_index = list(self.datetime_index_df.columns).index(self.fc_column)
            y_pred = y_pred[:, forecast_column_index]
            
        y_pred = self.transform_forecast(y_pred)
        return y_pred
 
    def cross_validate_single(self, lag_order, forecast_length):
        test_date = self.datetime_index_df.index[-1]
        forecast_date = test_date - pd.Timedelta(days = forecast_length)
        
        #test_df = self.datetime_index_df.copy()
        self.datetime_index_df = self.prep_data_for_forecast(self.datetime_index_df)
        test_df = self.datetime_index_df.copy()
        self.datetime_index_df = self.datetime_index_df.loc[self.datetime_index_df.index < forecast_date]
        
        y_pred = self.make_forecast(lag_order = lag_order, 
                                   forecast_length = forecast_length,
                                   prep_data = False)

        forecast_column_index = list(self.datetime_index_df.columns).index(self.fc_column) 
        y_true = test_df.loc[(test_df.index >= forecast_date) &
                            (test_df.index < test_date),
                            test_df.columns[forecast_column_index]].values
        y_true = self.transform_forecast(y_true)
        error = calc_RMSLE(y_pred, y_true)
        return (error, y_pred, y_true, self.datetime_index_df)