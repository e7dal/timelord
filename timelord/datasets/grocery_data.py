from os.path import dirname
import pandas as pd
#data from https://www.kaggle.com/c/favorita-grocery-sales-forecasting

class load_grocery(object):

    
    def __init__(self):
        self.path = dirname(__file__)
        self.items = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_items.csv')
        self.stores = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_stores.csv')
        self.holidays = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_holidays.csv')
        self.store_sales = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_store_sales.csv')
        self.item_sales = pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_item_sales.csv')
        self.long_multi_var_data = self.reload_df(pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_long_multi_var_data.csv'))
        self.short_multi_var_data = self.reload_df(pd.read_csv(dirname(__file__) + '/data/grocery_sales/grocery_short_multi_var_data.csv'))
        
        
    def reload_df(self, df):
        df.index = pd.DatetimeIndex(df['join_column_name'])
        return df.drop('join_column_name', axis = 1)
    
    
