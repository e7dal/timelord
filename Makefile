.PHONY: clean-pyc clean-build docs clean

help:
	@echo "clean - remove all build, test, coverage and Python artifacts"
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "clean-test - remove test and coverage artifacts"
	@echo "lint - check style with flake8"
	@echo "behave - behave tests"
	@echo "coverage - check code coverage quickly with the default Python"
	@echo "docs - generate Sphinx HTML documentation, including API docs"
	@echo "upload_to_pypi - upload a release"
	@echo "upload_to_testpypi - upload a release to testpypi"
	@echo "compile_requirements - pip compile the requirements"
	@echo "version - upDATE version"
	@echo "dist - package"
	@echo "compile_requirements - pip compile  *requirement.txt files from the *.in files"


clean: clean-build clean-pyc clean-test

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/

lint:
	flake8 timelord

behave:
	behave

test:
	#python setup.py test
	echo "no unit tests :( use behave :)"

coverage:
	coverage run --source timelord -m behave
	coverage report -m
	coverage html
	open htmlcov/index.html


version:
	echo `date +%Y.%m.%d` >VERSION.txt
	sed -i "s/^version =.*/version = \"`date +%Y.%m.%d`\"/" timelord/metadata.py

dist: clean docs
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

install: clean
	python setup.py bdist_wheel
	pip install ./dist/timelord-*-py2.py3-none-any.whl


compile_requirements:
	pip-compile requirements.in >requirements.txt
	pip-compile dev_requirements.in >dev_requirements.txt
	pip-compile requirements.in dev_requirements.in --output-file ci_requirements.txt
