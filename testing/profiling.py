# -*- coding: utf-8 -*-
# Part of timelord. See LICENSE file for full copyright and licensing details.
try:
    from cProfile import Profile
except ImportError:
    from profile import Profile
from pstats import Stats
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

TIMESERIES_PROFILE=False

def start_profile(*argv):
    # https://docs.python.org/3.4/library/profile.html#module-cProfile
    print('TIMESERIES_PROFILE:start_profile')
    global TIMESERIES_PROFILE
    TIMESERIES_PROFILE = Profile()
    TIMESERIES_PROFILE.enable()

def write_profile(pfile='./logs/profile.out'):
    global TIMESERIES_PROFILE
    if not TIMESERIES_PROFILE:
        return
    TIMESERIES_PROFILE.disable()
    #s = io.StringIO()
    s = StringIO()
    sortby = 'cumulative'
    ps = Stats(TIMESERIES_PROFILE,stream=s).sort_stats(sortby)
    ps.print_stats()
    # print(s.getvalue())
    # now=arrow.now()
    #pstats_file='./logs/profiling'+str(now)+'.pstats'
    #profile_text='./logs/profile'+str(now)+'.txt'
    pstats_file='./logs/profiling.pstats'
    profile_text='./logs/profile.txt'

    TIMESERIES_PROFILE.dump_stats(pstats_file)

    with open(profile_text,'a+') as pf:
        pf.write(s.getvalue())
    print("end_profile")
    print('TIMESERIES_PROFILE:pstats_file:'+pstats_file)
    print('TIMESERIES_PROFILE:profile_text:'+profile_text)

if __name__=='__main__':
    start_profile()
    def ok():pass
    ok()
    write_profile()
