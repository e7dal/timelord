import coloredlogs
print('original colored.DEFAULT_LOG_FORMAT')
print(coloredlogs.DEFAULT_LOG_FORMAT)
print('setting to format to a stupid default format for easy comparison from a behave feature')
coloredlogs.DEFAULT_LOG_FORMAT = 'NOTIME NOHOSTNAME %(name)s[NOPROCES] %(levelname)s %(message)s'


import timelord as tl

logger=tl.log.logger

# Some examples.
logger.debug("this is a debugging message")
logger.info("this is an informational message")
logger.warning("this is a warning message")
logger.error("this is an error message")
logger.critical("this is a critical message")

